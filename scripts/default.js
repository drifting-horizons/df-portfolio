﻿/// <reference path="jquery-3.3.1.js" />

var milliseconds = 1000;
var opacity = 0.5;

function displayCoverAsync() {
    return $('#cover').fadeTo(milliseconds, opacity).promise();
}

function showMessageContentASync(message) {
    $('#message').html(message);
    $('#messageBox').show();
    return $('#messageContent').slideDown(milliseconds).promise();

}

function showMessageASync(message) {
    var coverPromise = displayCoverAsync();
    var messagePromise = coverPromise.pipe(function () {
        return showMessageContentASync(message);
    });
    return messagePromise;
}
function displayTimeASync() {
    var message = 'The time is now ' + getTime();
    return showMessageASync(message);
}

function getTime() {
    var dateTime = new Date();
    var hours = dateTime.getHours();
    var minutes = dateTime.getMinutes();
    return hours + ':' + (minutes > 10 ? '0' + minutes : minutes);
}

function hideMessageContentASync() {
    var promise = $('#messageContent').slideUp(milliseconds).promise();
    promise.done(function () {
        $('#messageBox').hide();
    });
    return promise;
}

function hideCoverASync() {
    $('#cover').fadeOut(milliseconds).promise();
}

function hideMessageASync() {
    var messagePromise = hideMessageContentASync();
    var coverPromise = messagePromise.pipe(function () {
        return hideCoverASync();
    });
    return coverPromise;
}

$(document).ready(function () {
    $('#btnShowMessage').click(displayTimeASync);
    $('#btnOK').click(hideMessageASync);

});
